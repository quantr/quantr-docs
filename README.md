# Quantr Docs introductions

It is a documentation engine based on WordPress. Reason we build our own system is, we want flexible. All the docs are hosted in gitlab and this engine render them on WordPress.

# Test it and code it

use node v14.21.2

1. cd public/app
2. npm run watch
3. open vscode, install "sftp" extension, then the compiled js and css will automatically upload to https://docs.quantr.hk

# Install to your wordpress

1. git clone https://gitlab.com/quantr/quantr-docs.git
2. copy everything to your wordpress plugin directory, such as path wordpress/wp-content/plugins/quantr-docs
3. cd to quantr-docs/public/app, run "npm run build"
4. add a page in wordpress, add a short code to it [quantr_docs]
5. tune the setting in a file call public/html.php:

	var myConfig = {
		homeUrl		:	"https://docs.quantr.hk",
		loginUrl	:	"https:///wp-login.php?redirect_to=https://docs.quantr.hk",
	};

# Author

Peter <peter@quantr.hk>, System Architect, Quantr Limited

# Screens

![](https://www.quantr.foundation/wp-content/uploads/2023/04/QF-Docs-Screenshot-2023-04-06-at-5.46.12-PM.png)

![](https://www.quantr.foundation/wp-content/uploads/2023/04/QF-Docs-Screenshot-2023-04-06-at-5.46.19-PM.png)

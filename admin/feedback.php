<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/jquery/jquery-3.3.1.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/popper/popper.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/js/bootstrap.min.js'></script>
<link href='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/css/bootstrap.min.css' type='text/css' rel='stylesheet' />
<link href='<?= plugin_dir_url(__FILE__) ?>/admin.css' type='text/css' rel='stylesheet' />
<link href="<?= plugin_dir_url(__FILE__) ?>/image/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet" />

<h2 class="header1">Feedback</h2>
<script>
	function showDialog() {
		$('#addProjectModal #name').val(null);
		$('#addProjectModal #url').val(null);
		$('#addProjectModal #gitUrl').val(null);
		$('#addProjectModal #description').val(null);
		$('#addProjectModal #icon').val(null);
		$('#addProjectModal #types').val(null);
		$('#addProjectModal #enable').prop('checked', false);
		$('#addProjectModal #debug').prop('checked', false);
		$('#addProjectModal').modal('show');
	}

	function approve(id) {
		$.post("<?= plugin_dir_url(__FILE__) ?>/feedbackApi.php", {
			'type': 'approve',
			'id': id
		}, function(res) {
			if (res != 'yes') {
				alert('failed to approve project, ' + res);
			} else {
				location.reload();
			}
		});
	}

	function reject(id) {
		$.post("<?= plugin_dir_url(__FILE__) ?>/feedbackApi.php", {
			'type': 'reject',
			'id': id
		}, function(res) {
			if (res != 'yes') {
				alert('failed to reject project, ' + res);
			} else {
				location.reload();
			}
		});
	}
</script>
<table class="table">
	<thead>
		<tr>
			<th>id</th>
			<th>date</th>
			<th>username</th>
			<th style="width: 500px;">content</th>
			<th>doc path</th>
			<th>status</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?
		global $wpdb;
		$results = $wpdb->get_results("SELECT * FROM " . qd_feedback . " order by date desc", OBJECT);

		foreach ($results as &$row) {
			echo '<td class="align-middle">' . $row->id . '</div></td>';
			echo '<td class="align-middle">' . $row->date . '</div></td>';
			echo '<td class="align-middle">' . $row->username . '</div></td>';
			echo '<td class="align-middle">' . $row->content . '</div></td>';
			echo '<td class="align-middle"><a href="https://www.quantr.hk/docs/?project=' . $row->project . '&docPath=' . $row->docPath . '&url=' . $row->url . '" target="_blank">' . $row->docPath . '<a/></div></td>';
			echo '<td class="align-middle">' . ($row->status ? 'true' : 'false') . '</div></td>';
			echo '<td nowrap style="vertical-align: middle;">';
			echo '<button type="button" class="btn btn-success" onclick="if (confirm(\'Confirm approve ?\')){approve(' . $row->id . ');}">Approve</button> ';
			echo '<button type="button" class="btn btn-danger" onclick="if (confirm(\'Confirm reject ?\')){reject(' . $row->id . ');}">Reject</button> ';
			echo '</td>';
			echo '</tr>';
		}
		?>
	</tbody>
</table>
<div class="modal" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="addProjectModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add project</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="uploadForm" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Name</label>
						<div class="col"><input type="text" id="name" name="name" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Url</label>
						<div class="col"><input type="text" id="url" name="url" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Git url</label>
						<div class="col"><input type="text" id="gitUrl" name="gitUrl" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Description</label>
						<div class="col"><input type="text" id="description" name="description" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col"><input type="text" id="icon" name="icon" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Types</label>
						<div class="col"><input type="text" id="types" name="types" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Doc folder</label>
						<div class="col"><input type="text" id="docFolder" name="docFolder" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Enable</label>
						<div class="col"><input type="checkbox" id="enable" name="enable" class="form-control" value="1" /></div>
						<label class="col-sm-2 col-form-label">Debug</label>
						<div class="col"><input type="checkbox" id="debug" name="debug" class="form-control" value="1" /></div>
					</div>
					<input type="hidden" name="type" value="create" />
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="submitForm();">Save</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="editProjectModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="uploadForm" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Name</label>
						<div class="col"><input type="text" id="name" name="name" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Url</label>
						<div class="col"><input type="text" id="url" name="url" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Git url</label>
						<div class="col"><input type="text" id="gitUrl" name="gitUrl" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Description</label>
						<div class="col"><input type="text" id="description" name="description" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col"><input type="text" id="icon" name="icon" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Types</label>
						<div class="col"><input type="text" id="types" name="types" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Doc folder</label>
						<div class="col"><input type="text" id="docFolder" name="docFolder" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Enable</label>
						<div class="col"><input type="checkbox" id="enable" name="enable" class="form-control" value="1" /></div>
						<label class="col-sm-2 col-form-label">Debug</label>
						<div class="col"><input type="checkbox" id="debug" name="debug" class="form-control" value="1" /></div>
					</div>
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="type" value="edit" />
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="submitEditForm();">Save</button>
			</div>
		</div>
	</div>
</div>
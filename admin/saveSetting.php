<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
global $wpdb;

foreach ($_POST as $key => $value) {
	$wpdb->delete(qd_setting, array('property' => $key));

    $wpdb->insert(
        qd_setting,
        array(
			'property' => $key,
			'value' => $value
		)
    );
}

echo "yes";

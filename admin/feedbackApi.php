<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
global $wpdb;

if ($_POST['type'] == 'approve') {
	$wpdb->update(
		qd_feedback,
		array(
			'status' => 1
		),
		array('ID' => $_POST['id'])
	);
	echo "yes";
} elseif ($_POST['type'] == 'reject') {
	$wpdb->update(
		qd_feedback,
		array(
			'status' => 0
		),
		array('ID' => $_POST['id'])
	);
	echo "yes";
}

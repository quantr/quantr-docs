<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
global $wpdb;

if ($_POST['type'] == 'create') {
	$file = $_FILES["icon"]["tmp_name"];
	$size = filesize($file);
	if ($size > 200 * 1024) {
		echo "File too large";
	} else {
		$wpdb->insert(
			qd_project,
			array(
				'name' => $_POST['name'],
				'url' => $_POST['url'],
				'gitUrl' => $_POST['gitUrl'],
				'description' => $_POST['description'],
				'icon' => $_POST['icon'],
				'types' => $_POST['types'],
				'docFolder' => $_POST['docFolder'],
				'enable' => $_POST['enable'] == "true",
				'debug' => $_POST['debug'] == "true",
				'createDate' => current_time('mysql'),
			)
		);
		echo "yes";
	}
} else if ($_POST['type'] == 'delete') {
	$wpdb->delete(qd_project, array('id' => $_POST['id']));
	echo "yes";
} else if ($_POST['type'] == 'get') {
	$row = $wpdb->get_row($wpdb->prepare("select * from " . qd_project . " where id=%d", $_POST['id']));
	echo wp_json_encode($row);
} else if ($_POST['type'] == 'edit') {
	$wpdb->update(
		qd_project,
		array(
			'name' => $_POST['name'],
			'url' => $_POST['url'],
			'gitUrl' => $_POST['gitUrl'],
			'description' => $_POST['description'],
			'icon' => $_POST['icon'],
			'types' => $_POST['types'],
			'docFolder' => $_POST['docFolder'],
			'enable' => $_POST['enable'] == "true",
			'debug' => $_POST['debug'] == "true",
		),
		array('ID' => $_POST['id'])
	);
	echo "yes";
}

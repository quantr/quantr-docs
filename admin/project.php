<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/jquery/jquery-3.3.1.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/popper/popper.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/js/bootstrap.min.js'></script>
<link href='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/css/bootstrap.min.css' type='text/css' rel='stylesheet' />
<link href='<?= plugin_dir_url(__FILE__) ?>/admin.css' type='text/css' rel='stylesheet' />
<link href="<?= plugin_dir_url(__FILE__) ?>/image/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet" />

<h2 class="header1">Project</h2>
<button type="button" class="btn btn-primary" onclick='showDialog();'>Add project</button><br>
<br>
<script>
	function showDialog() {
		$('#addProjectModal #name').val(null);
		$('#addProjectModal #url').val(null);
		$('#addProjectModal #gitUrl').val(null);
		$('#addProjectModal #description').val(null);
		$('#addProjectModal #icon').val(null);
		$('#addProjectModal #types').val(null);
		$('#addProjectModal #enable').prop('checked', false);
		$('#addProjectModal #debug').prop('checked', false);
		$('#addProjectModal').modal('show');
	}

	function submitForm() {
		var formData = new FormData($('#addProjectModal #uploadForm')[0]);
		var checkbox = $("#addProjectModal #uploadForm").find("input[type=checkbox]");
		$.each(checkbox, function(key, val) {
			formData.append($(val).attr('name'), $(this).is(":checked"))
		});
		$.ajax({
			url: '<?= plugin_dir_url(__FILE__) ?>/createProject.php',
			type: 'POST',
			cache: false,
			data: formData,
			processData: false,
			contentType: false
		}).done(function(res) {
			console.log(res);
			if (res != 'yes') {
				alert('failed to add project, ' + res);
				$('#addProjectModal').modal('hide');
			} else {
				location.reload();
			}
		}).fail(function(res) {
			alert('failed to create user');
			console.log(res);
		});
	}

	function submitEditForm() {
		var formData = new FormData($('#editProjectModal #uploadForm')[0]);
		var checkbox = $("#editProjectModal #uploadForm").find("input[type=checkbox]");
		$.each(checkbox, function(key, val) {
			formData.append($(val).attr('name'), $(this).is(":checked"))
		});
		$.ajax({
			url: '<?= plugin_dir_url(__FILE__) ?>/createProject.php',
			type: 'POST',
			cache: false,
			data: formData,
			processData: false,
			contentType: false
		}).done(function(res) {
			if (res != 'yes') {
				alert('failed to edit project, ' + res);
				$('#editProjectModal').modal('hide');
			} else {
				location.reload();
			}
		}).fail(function(res) {
			alert('failed to create project');
			console.log(res);
		});
	}

	function deleteProject(id) {
		$.post("<?= plugin_dir_url(__FILE__) ?>/createProject.php", {
			'type': 'delete',
			'id': id
		}, function(res) {
			if (res != 'yes') {
				alert('failed to delete project, ' + res);
			} else {
				location.reload();
			}
		});
	}

	function editProject(id) {
		$.post("<?= plugin_dir_url(__FILE__) ?>/createProject.php", {
			'type': 'get',
			'id': id
		}, function(res) {
			var json = JSON.parse(res);
			$('#editProjectModal #id').val(id);
			$('#editProjectModal #name').val(json.name);
			$('#editProjectModal #url').val(json.url);
			$('#editProjectModal #gitUrl').val(json.gitUrl);
			$('#editProjectModal #description').val(json.description);
			$('#editProjectModal #icon').val(json.icon);
			$('#editProjectModal #types').val(json.types);
			$('#editProjectModal #docFolder').val(json.docFolder);
			$('#editProjectModal #enable').prop('checked', json.enable == 1 ? true : false);
			$('#editProjectModal #debug').prop('checked', json.debug == 1 ? true : false);
			$('#editProjectModal').modal('show');
		});
	}
</script>
<table class="table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Url</th>
			<th>Create Date</th>
			<!-- <th>Description</th> -->
			<th>Enable</th>
			<th>Type</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?
		global $wpdb;
		$results = $wpdb->get_results("SELECT * FROM " . qd_project . " order by name", OBJECT);

		foreach ($results as &$row) {
			$ext = pathinfo($row->iconName, PATHINFO_EXTENSION);

			$output = "";
			if ($row->types != null) {
				$types = explode(',', $row->types);
				foreach ($types as $type) {
					$output .= '<span class="tag">' . $type . '</span><br />';
				}
			}

			echo '<tr>';
			if ($row->icon == null) {
				echo '<td class="align-middle" nowrap>' . $row->name . '</td>';
			} else {
				$str = $row->icon;
				$str = substr($str, 0, 2) . strtolower(substr($str, 2, 1)) . substr($str, 3);
				echo '<td class="align-middle"><i class="fas ' . str_replace('fa', 'fa-', $str) . '"></i> ' . $row->name . '</td>';
			}
			echo '<td class="align-middle">' . $row->url . '<br>' . $row->gitUrl . '</div></td>';
			echo '<td class="align-middle">' . $row->createDate . '</div></td>';
			// echo '<td class="align-middle">'.$row->description.'</div></td>';
			echo '<td class="align-middle">' . ($row->enable ? "yes" : "no") . '</div></td>';
			echo '<td class="align-middle">' . $output . '</div></td>';
			echo '<td nowrap>';
			echo '<button type="button" class="btn btn-success" onclick="editProject(' . $row->id . ');">Edit</button> ';
			echo '<button type="button" class="btn btn-danger" onclick="if (confirm(\'Confirm delete ?\')){deleteProject(' . $row->id . ');}">Delete</button> ';
			echo '</td>';
			echo '</tr>';
		}
		?>
	</tbody>
</table>
<div class="modal" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="addProjectModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Add project</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="uploadForm" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Name</label>
						<div class="col"><input type="text" id="name" name="name" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Url</label>
						<div class="col"><input type="text" id="url" name="url" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Git url</label>
						<div class="col"><input type="text" id="gitUrl" name="gitUrl" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Description</label>
						<div class="col"><input type="text" id="description" name="description" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col"><input type="text" id="icon" name="icon" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Types</label>
						<div class="col"><input type="text" id="types" name="types" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Doc folder</label>
						<div class="col"><input type="text" id="docFolder" name="docFolder" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Enable</label>
						<div class="col"><input type="checkbox" id="enable" name="enable" class="form-control" value="1" /></div>
						<label class="col-sm-2 col-form-label">Debug</label>
						<div class="col"><input type="checkbox" id="debug" name="debug" class="form-control" value="1" /></div>
					</div>
					<input type="hidden" name="type" value="create" />
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="submitForm();">Save</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="editProjectModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="uploadForm" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Name</label>
						<div class="col"><input type="text" id="name" name="name" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Url</label>
						<div class="col"><input type="text" id="url" name="url" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Git url</label>
						<div class="col"><input type="text" id="gitUrl" name="gitUrl" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Description</label>
						<div class="col"><input type="text" id="description" name="description" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Icon</label>
						<div class="col"><input type="text" id="icon" name="icon" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Types</label>
						<div class="col"><input type="text" id="types" name="types" class="form-control" /></div>
						<label class="col-sm-2 col-form-label">Doc folder</label>
						<div class="col"><input type="text" id="docFolder" name="docFolder" class="form-control" /></div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Enable</label>
						<div class="col"><input type="checkbox" id="enable" name="enable" class="form-control" value="1" /></div>
						<label class="col-sm-2 col-form-label">Debug</label>
						<div class="col"><input type="checkbox" id="debug" name="debug" class="form-control" value="1" /></div>
					</div>
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="type" value="edit" />
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" onclick="submitEditForm();">Save</button>
			</div>
		</div>
	</div>
</div>
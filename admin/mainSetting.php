<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/jquery/jquery-3.3.1.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/popper/popper.min.js'></script>
<script type='text/javascript' src='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/js/bootstrap.min.js'></script>
<link href='<?= plugin_dir_url(__FILE__) ?>/library/bootstrap/css/bootstrap.min.css' type='text/css' rel='stylesheet' />
<link href='<?= plugin_dir_url(__FILE__) ?>/admin.css' type='text/css' rel='stylesheet' />
<link href="<?= plugin_dir_url(__FILE__) ?>/image/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet" />

<?
	global $wpdb;
	$results = $wpdb->get_results("SELECT * FROM " . qd_setting, OBJECT);

	$arr=array();
	foreach ($results as &$row) {
		// echo $row->property;
		$arr[$row->property]=$row->value;
	}
?>

<script>
	function save(){
		var formData = new FormData($('#settingForm')[0]);
		var checkbox = $("#settingForm").find("input[type=checkbox]");
		$.each(checkbox, function(key, val) {
			formData.append($(val).attr('name'), $(this).is(":checked"))
		});
		$.ajax({
			url: '<?= plugin_dir_url(__FILE__) ?>/saveSetting.php',
			type: 'POST',
			cache: false,
			data: formData,
			processData: false,
			contentType: false
		}).done(function(res) {
			console.log(res);
			if (res != 'yes') {
				alert('failed to save settings, ' + res);
				$('#addProjectModal').modal('hide');
			} else {
				location.reload();
			}
		}).fail(function(res) {
			alert('failed to save settings');
			console.log(res);
		});
	}
</script>
<form id="settingForm" enctype="multipart/form-data">
	<div class="container">
		<div class="row">
			<div class="col">
				<strong>Setting</strong>
			</div>
		</div>
		<div class="row">
			<div class="col">
				Show header
			</div>
			<div class="col">
				<input type="checkbox" name="showHeader" <? if ($arr['showHeader']=="true"){ ?> checked <? } ?> />
			</div>
		</div>
		<div class="row">
			<div class="col">
				Social button color
			</div>
			<div class="col">
				<input type="color" name="socialButtonColor" value="<?= $arr['socialButtonColor'] ?>" />
			</div>
		</div>
		<div class="row">
			<div class="col">
				Button color
			</div>
			<div class="col">
				Fore color <input type="color" name="buttonColor" value="<?= $arr['buttonColor'] ?>" />
				Background color <input type="color" name="buttonBackgroundColor" value="<?= $arr['buttonBackgroundColor'] ?>" />
				Border color <input type="color" name="buttonBorderColor" value="<?= $arr['buttonBorderColor'] ?>" />
			</div>
		</div>
		<div class="row">
			<div class="col">
				Header background
			</div>
			<div class="col">
				<input type="text" name="headerBackground" value="<?= $arr['headerBackground'] ?>" />
			</div>
		</div>
		<div class="row">
			<div class="col">
				Header color
			</div>
			<div class="col">
				<input type="color" name="headerColor" value="<?= $arr['headerColor'] ?>" />
			</div>
		</div>
		<div class="row">
			<div class="col">
				<button type="button" class="btn btn-success" onClick="save()">Save</button>
			</div>
		</div>
	</div>
</div>

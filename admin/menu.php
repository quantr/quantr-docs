<?
function qd_create_menu(){
    // $first=plugin_dir_path(__FILE__) . 'mainSetting.php';
    add_menu_page(
        'Quantr Docs',
        'Quantr Docs',
        'manage_options',
        'qd_mainMenuSlug',
        'sharepoint main menu',
        plugin_dir_url(__FILE__) . 'image/logos_purple.png'
    );

    add_submenu_page(
        'qd_mainMenuSlug',
        'Setting',
        'Setting',
        'manage_options',
        plugin_dir_path(__FILE__) . '/mainSetting.php',
        null
    );

    add_submenu_page(
        'qd_mainMenuSlug',
        'Project',
        'Project',
        'manage_options',
        plugin_dir_path(__FILE__) . '/project.php',
        null
    );

    add_submenu_page(
        'qd_mainMenuSlug',
        'Feedback',
        'Feedback',
        'manage_options',
        plugin_dir_path(__FILE__) . '/feedback.php',
        null
    );

    // add_submenu_page(
    //     'qd_mainMenuSlug',
    //     'Product',
    //     'Product',
    //     'manage_options',
    //     plugin_dir_path(__FILE__) . '/product.php',
    //     null
    // );

	remove_submenu_page('qd_mainMenuSlug', 'qd_mainMenuSlug');
}

add_action('admin_menu', 'qd_create_menu');

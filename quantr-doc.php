<?
/*
Plugin Name: Quantr Docs
Plugin URI: https://gitlab.com/quantr/quantr-docs
Description: Documentation engine for wordpress
Version: 1.2.0
Author: Peter
Support URI: https://gitlab.com/quantr/quantr-docs
Author URI: https://gitlab.com/quantr/quantr-docs
License: LGPL

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

require_once('vendor/autoload.php');
//include(get_home_path() . '/wp-includes/pluggable.php');
include(ABSPATH . '/wp-includes/pluggable.php');
include(plugin_dir_path(__FILE__) . 'public/shortcode.php');
include(plugin_dir_path(__FILE__) . 'admin/menu.php');

// define("QUANTR_DOCS_FUCK", '123Peter456');

// add_action('init', 'quantr_docs_rules');
// function quantr_docs_rules()
// {
// 	global $wp_rewrite;
// 	//    add_rewrite_rule('docs/project/(.+)','docs?project=$1 [R,L]', 'top');
// 	//    add_rewrite_rule('wp-content/(.+)','https://www.quantr.hk/wp-content/$1 [R,L]', 'top');
// 	$wp_rewrite->flush_rules();
// }

// add_filter("pre_get_document_title", "setTitle");
// function setTitle($old_title)
// {
// 	return "Quantr Docs";
// }

global $wpdb;
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
$charset_collate = $wpdb->get_charset_collate();

define('QUANTR_DOCS_TABLE_PREFIX', 'quantr_docs_');

define('qd_project', QUANTR_DOCS_TABLE_PREFIX.'project');
$sql = "CREATE TABLE " . qd_project . " (
	id int(9) NOT NULL AUTO_INCREMENT,
	createDate datetime NULL,
	name varchar(100) NOT NULL,
	url varchar(1024) NOT NULL,
	gitUrl varchar(1024) NOT NULL,
	description varchar(200) NOT NULL,
	icon varchar(100) NOT NULL,
	types varchar(200) NULL,
	docFolder varchar(200) NULL,
	enable boolean,
	debug boolean,
	PRIMARY KEY (id)
) $charset_collate;";
// dbDelta($sql);

define('qd_setting', QUANTR_DOCS_TABLE_PREFIX.'setting');
$sql = "CREATE TABLE " . qd_setting . " (
	id int(9) NOT NULL AUTO_INCREMENT,
	property varchar(100) NOT NULL,
	value varchar(1000) NOT NULL,
	PRIMARY KEY (id)
) $charset_collate;";
// dbDelta($sql);

define('qd_feedback', QUANTR_DOCS_TABLE_PREFIX.'feedback');
$sql = "CREATE TABLE " . qd_feedback . " (
	id int(9) NOT NULL AUTO_INCREMENT,
	username varchar(100) NOT NULL,
	content varchar(102400) NULL,
	url varchar(10240) NULL,
	project varchar(1024) NULL,
	docPath varchar(1024) NULL,
	date datetime NOT NULL,
	status boolean ,
	PRIMARY KEY (id)
) $charset_collate;";
// dbDelta($sql);

// add_action('parse_request', 'qd_url_handler');
// function qd_url_handler()
// {
// 	$url = str_replace('%20', '', $_SERVER["REQUEST_URI"]);
// 	if (strpos($url, '/thedb/') === 0) {
// 		header('Location: https://' . $_SERVER['HTTP_HOST'] . '/project/?project=thedb');
// 	}
// }

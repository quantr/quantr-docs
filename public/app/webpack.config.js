'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

let config = {
    entry: {
        main: [
            './_devapp/app.js',
            './_devapp/css/app.css'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'assets', 'bundle'),
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
    },
    devtool: "source-map",
    module: {
        rules: [{
                test: /\.(js|jsx|tsx|ts)$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                            '@babel/preset-typescript'
                        ],
                        plugins: [
                            ["@babel/plugin-proposal-decorators", {
                                "legacy": true
                            }],
                            '@babel/plugin-syntax-dynamic-import', ['@babel/plugin-proposal-class-properties', {
                                "loose": true
                            }]
                        ]
                    }
                },
            },
            // {
            // 	test: /\.scss$/,
            // 	use: ExtractTextPlugin.extract({
            // 		fallback: 'style-loader',
            // 		use: [
            // 			{
            // 				loader: 'css-loader',
            // 			},
            // 			'postcss-loader',
            // 			'sass-loader'
            // 		]
            // 	})
            // },
            {
                test: /\.scss$/,
                use: [{
                        loader: "style-loader",
                        options: {
                            sourceMap: true
                        } // creates style nodes from JS strings
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        } // translates CSS into CommonJS
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        } // compiles Sass to CSS
                    }
                ]
            },
            {
                test: /.(png|woff(2)?|eot|ttf|svg|gif)(\?[a-z0-9=\.]+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '/img/[hash].[ext]',
                        // publicPath: function (url) {
                        // 	return "https://www.quantr.hk/wp-content/plugins/quantr-docs/public/app/assets/bundle/" + url;
                        // },
                    }
                }]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader', 'postcss-loader']
            }
        ]
    },
    externals: {
        plugin_dir_url: 'plugin_dir_url',
        userCredential: 'userCredential',
        projects: 'projects',
        project: 'project',
        myConfig: 'myConfig'
    },
    plugins: [
        new ExtractTextPlugin(path.join('..', 'css', 'app.css')),
        new webpack.DefinePlugin({
            //'__DEV__': JSON.stringify(true),
            //'__API_HOST__': JSON.stringify('https://www.quantr.hk/docs/'),
            //'process.env.ASSET_PATH': JSON.stringify('https://www.quantr.hk/wp-content/plugins/quantr-docs/')
        }),
    ],
};

if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            compress: {
                sequences: true,
                conditionals: true,
                booleans: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            },
            output: {
                comments: false
            },
            minimize: true
        })
    );
}

module.exports = config;
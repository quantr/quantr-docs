import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import './Feedback.module.scss';
import userCredential from 'userCredential';
import * as $ from 'jquery';
import myConfig from 'myConfig';
const axios = require('axios');
var moment = require('moment');
// import '@fortawesome/fontawesome-free/css/all.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComment } from '@fortawesome/free-solid-svg-icons'
export default class Feedback extends Component {
	feedbacks = [];

	state = {
		docPath: this.props.docPath
	};

	submitFeedback = () => {
		let feedbackContent = this.refs['feedbackContent'];
		var data = new FormData();
		var that = this;
		data.append('action', 'submitFeedback');
		data.append('url', this.props.url);
		data.append('project', this.props.project);
		data.append('docPath', this.props.docPath);
		data.append('feedbackContent', feedbackContent.value);
		axios.post(plugin_dir_url + '/api.php', data).then(function (response) {
			console.log(response.data);
			feedbackContent.value = null;
			that.loadFeedbacks();
		}).catch(function (error) {
			console.log(error);
		});
	}

	loadFeedbacks() {
		var that = this;
		console.log('===' + plugin_dir_url + '/api.php?action=getFeedbacks&project=' + this.props.project + '&url=' + this.props.url + '&docPath=' + this.state.docPath);
		axios.get(plugin_dir_url + '/api.php?action=getFeedbacks&project=' + this.props.project + '&url=' + this.props.url + '&docPath=' + this.state.docPath).then(function (response) {
			console.log(response.data);
			// let items = JSON.parse(response.data);
			let items = response.data;
			that.feedbacks = [];
			that.forceUpdate();
			for (var x = 0; x < items.length; x++) {
				console.log(items[x]);
				var a = moment();
				var b = moment(items[x].date);
				that.feedbacks.push(<div key={x} className="container feedbackContainer">
					<div className="row feedbackHeader">
						<div className="col">{items[x].username}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{a.diff(b, 'days')} days</div>
					</div>
					<div className="row feedbackContent">
						<div className="col" style={{ margin: '5px' }}>{items[x].content}</div>
					</div>
					<div className="row feedbackReply">
						<div className="col">
							<span className="feedbackReplyButton"><FontAwesomeIcon icon={faComment} /> Reply</span>
						</div>
					</div>
				</div>);
			}
			that.forceUpdate();
		});
	}

	componentDidMount() {
		this.loadFeedbacks();
	}

	render() {
		return <div id="feedback" className="container-fluid" style={{ paddingLeft: '0px', paddingRight: '0px' }}>
			<div className="row">
				<div className="col-12">
					<h1>Feedback</h1>
				</div>
			</div>
			{userCredential.email == null &&
				<div className="row">
					<div className="col-12">
						<div className="alert alert-danger" role="alert">
							Please login first
						</div>
					</div>
				</div>
			}
			{userCredential.email != null &&
				<div className="row">
					<div className="col-12">
						<textarea ref="feedbackContent" className="form-control" rows={6} style={{ marginBottom: '5px' }}></textarea>
						<button type="button" className="btn btn-purple" style={{ color: myConfig.settings.color, backgroundColor: myConfig.settings.buttonBackgroundColor, borderColor: myConfig.settings.buttonBorderColor }} onClick={this.submitFeedback}>Post</button>
					</div>
				</div>
			}
			<div className="row" style={{ marginTop: '5px' }}>
				<div className="col-12">
					{this.feedbacks}
				</div>
			</div>
		</div>;
	}
}

import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import projects from 'projects';
import 'bootstrap/dist/css/bootstrap.css';
import './Main.module.scss';
import Header from './Header.jsx';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';

export default class Main extends Component {

	projectsTags = [];

	componentDidMount() {
		this.search();
	}

	search = () => {
		this.projectsTags = [];
		var str = $('#searchTextField').val();
		for (var x = 0; x < projects.length; x++) {
			var typeTags = [];
			var bingo = false;
			for (var y = 0; y < projects[x].types.length; y++) {
				typeTags.push(
					<span key={y} className="projectType">{projects[x].types[y]}</span>
				);
				if (str == null || str == '' || projects[x].types[y].toLowerCase().indexOf(str.toLowerCase()) != -1) {
					bingo = true;
				}
			}
			if (bingo || str == null || str == '' || projects[x].name.toLowerCase().indexOf(str.toLowerCase()) != -1) {
				let p = projects[x];
				console.log(p);
				if (projects[x].enable == "1") {
					this.projectsTags.push(
						<div key={x} className="col-md-4 col-sm-6 col-xs-12" style={{ padding: '10px' }}>
							<div className="projectBox" onClick={() => this.gotoProject(p.name, p.url)}>
								<table style={{ height: '56px' }}>
									<tbody>
										<tr>
											<td className="projectIconDiv"><FontAwesomeIcon icon={fontawesome[p.icon]} size="2x" className="projectIcon" /></td>
											<td className="projectName">{p.name}</td>
										</tr>
									</tbody>
								</table>
								<div className="projectDescription">{p.description}</div>
								<div className="projectTypes">{typeTags}</div>
							</div>
						</div>
					);
				} else {
					this.projectsTags.push(
						<div key={x} className="col-md-4 col-sm-6 col-xs-12" style={{ padding: '10px' }}>
							<div className="projectBox projectBoxDisable">
								<table style={{ height: '56px' }}>
									<tbody>
										<tr>
											<td className="projectIconDiv"><FontAwesomeIcon icon={fontawesome[p.icon]} size="2x" className="projectIcon" /></td>
											<td className="projectName">{p.name}</td>
										</tr>
									</tbody>
								</table>
								<div className="projectDescription">{p.description}</div>
								<div className="projectTypes">{typeTags}</div>
							</div>
						</div>
					);
				}
			}
		}
		console.log(this.projectsTags);
		this.forceUpdate();
	}

	gotoProject = (projectName, url) => {
		window.location = "www.quantr.hk/docs?project=" + projectName;
	}

	render() {
		return (
			<div className="main">
				{myConfig.settings.showHeader == "true" && <Header type="main" />}
				<div className="container projectsDiv">
					<div className="row pt-2 pb-2">
						<input id="searchTextField" className="form-control" type="text" placeholder="Search" onChange={this.search} />
					</div>
					<div className="row">
						{this.projectsTags}
					</div>
				</div>
			</div>
		)
	}
}

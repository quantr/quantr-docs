import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
// import projects from 'projects';
import 'bootstrap/dist/css/bootstrap.css';
import './Project.module.scss';
import Header from './Header.jsx';
import Feedback from './Feedback.jsx';
import userCredential from 'userCredential';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as fontawesome from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery';
const axios = require('axios');
import plugin_dir_url from 'plugin_dir_url';
import Tree from 'react-ui-tree';
import classnames from 'classnames';
import 'react-ui-tree/dist/react-ui-tree.css';
import 'jquery-query-object';
import '@iconfu/svg-inject/dist/svg-inject.js';
import {
	BrowserView,
	MobileView,
	isDesktop
} from "react-device-detect";

//global.__basedir = '/wp-content/plugins/quantr-docs/';
// global.__baseurl = "https://www.quantr.hk/wp-content/plugins/quantr-docs/";
global.__baseurl = "https://www.quantr.foundation/wp-content/plugins/quantr-docs/";

export default class Project extends Component {
	state = {
		active: null,
		tree: { module: 'Loading' },
		treeOptions: null
	};

	content = null;
	url = null;
	gitUrl = null;
	sectionName = null;
	initialzing = true;
	loading = false;
	docNotAvailable = false;
	docPath = null;

	componentDidMount() {
		//console.log(document.getElementsByClassName("injectable"));
		//SVGInject(document.getElementsByClassName("injectable"));
		//SVGInject(document.querySelectorAll("img.injectable"));

		var that = this;
		axios.get(plugin_dir_url + '/api.php?action=getProjectUrl&name=' + this.props.project).then(function (response) {
			console.log(response.data);
			// if (response.data[0].docFolder == null) {
			// 	that.gitUrl = response.data[0].gitUrl + '/raw/master';
			// } else {
			if (response.data[0].docFolder == null) {
				that.gitUrl = response.data[0].gitUrl;
			} else {
				that.gitUrl = response.data[0].gitUrl + '/' + response.data[0].docFolder;
			}
			// }
			var indexUrl = that.gitUrl + '/top.quantrdocs';
			console.log('indexUrl=' + indexUrl);
			var url = plugin_dir_url + '/api.php?action=getUrl&url=' + indexUrl;
			console.log(url);
			axios.get(url).then(function (response) {
				if (response.data == '') {
					that.docNotAvailable = true;
					that.initialzing = false;
					that.forceUpdate();
					return;
				}
				if (response.data.indexOf('error-404') != -1) {
					$('#debug').html("404 page not found");
					$('#debug').css('display', 'none');
					that.initialzing = false;
					return;
				}
				// $('#debug').append(response.data);
				var lines = response.data.split(/\r?\n/);
				var tree = {};
				var treeOptions = [];
				var lastLevelMap = new Map();

				var rootNode = {
					displayName: null,
					module: that.props.project,
					undraggable: true,
					children: []
				};

				lastLevelMap.set(-1, rootNode);
				for (var x = 0; x < lines.length; x++) {
					if (lines[x].trim() == '') {
						continue;
					}
					var noOfSpace = lines[x].search(/\S/);
					var tokens = lines[x].split('--');
					var displayName = tokens[0].trim().substring(2);
					var plusMinus = tokens[0].trim().substring(0, 1);
					var page = null;
					if (tokens.length > 1) {
						page = tokens[1].trim();
					}

					if (x == 0 && (jQuery.query.get("url") == null || jQuery.query.get("url") == '')) {
						that.loadPage(that.gitUrl + '/' + page, page, jQuery.query.get("section"));
					}

					var parentNode = lastLevelMap.get(noOfSpace - 1);
					// console.log(parentNode);

					// create node
					var node = {
						displayName: displayName,
						undraggable: true,
						collapsed: plusMinus == '-',
						docPath: page,
						children: []
					};

					if (that.gitUrl + "/" + page == jQuery.query.get("url")) {
						that.setState({
							active: node
						});
						that.sectionName = displayName;
					}

					parentNode.children.push(node);
					// console.log('set map = ' + noOfSpace);
					lastLevelMap.set(noOfSpace, node);
					// console.log(lastLevelMap);

					// $('#debug').append('>' + noOfSpace + ' = ' + lines[x] + '\n');
					console.log(displayName);

					treeOptions.push(
						<option value={node.docPath}>{displayName}</option>
					);
				}
				tree = rootNode;
				// console.log(tree);
				that.setState({
					tree: tree,
					treeOptions: treeOptions
				});

				console.log('jQuery.query.get("url")=' + jQuery.query.get("url"));
				if (jQuery.query.get("url") != null && jQuery.query.get("url") != '') {
					that.loadPage(jQuery.query.get("url"), jQuery.query.get("docPath"), jQuery.query.get("section"));
				}

				that.initialzing = false;
				//that.forceUpdate();
			});
		});
	}

	updateFeedback(docPath) {
		let feedback = this.refs['feedback'];
		console.log('feedback=' + feedback);
		if (feedback != null) {
			console.log('>>' + docPath);
			feedback.setState({ docPath: docPath }, function () {
				feedback.loadFeedbacks();
			}.bind(this));
		}
		this.forceUpdate();
	}

	loadPage(urlParam, docPath, scrollToSection) {
		$('#content').html('');
		this.url = urlParam;
		this.loading = true;
		this.docPath = docPath;
		this.forceUpdate();
		this.updateFeedback(docPath);
		document.title = "Quantr Docs - " + this.props.project + " - " + this.sectionName;
		console.log('urlParam=' + urlParam);
		var newerQuery = jQuery.query.SET("url", urlParam);
		window.history.pushState({ path: newerQuery }, '', newerQuery);
		var newerQuery2 = jQuery.query.SET("docPath", docPath);
		window.history.pushState({ path: newerQuery2 }, '', newerQuery2);
		var url = plugin_dir_url + '/api.php?action=getUrl&url=' + urlParam + '&type=markdown&docPath=' + docPath;
		var that = this;
		axios.get(url).then(function (response) {
			if (response.data.indexOf('error-404') != -1) {
				that.content = null;
				$('#content').html('Page not found');
				return;
			}
			$([document.documentElement, document.body]).animate({
				scrollTop: 0
			}, 0);
			that.content = response.data;
			that.loading = false;
			that.forceUpdate();
			$('#content').html(that.content);
			$('#content img').addClass('img-thumbnail');
			$("#content img").each(function() {
				var $this = $(this);
				var src = $this.attr('src');
				$this.addClass('image');
				var a = $('<a data-lightbox="image-1">').attr('href', src);
				$this.wrap(a);
			});
			// $('#content img').wrap( "<a href=\"https://www.quantr.foundation/wp-content/uploads/2023/04/Quantr-Logic-2023-04-03-at-2.25.30-AM.png\" data-lightbox=\"image-1\"></a>" );
			// $('#content a[target!=\'_blank\']:has(img)').attr('data-lightbox', 'image-1');
			that.updateArticleSections();

			if (scrollToSection != -1 && scrollToSection != null && scrollToSection != '') {
				// console.log('scrollToSection=' + scrollToSection);
				$([document.documentElement, document.body]).animate({
					scrollTop: $('#section_' + scrollToSection).offset().top
				}, 500);
			}

			$(window).scroll(function () {
				var html = $('#content h1,h2,h3,h4,h5,h6');

				var numberOfVisibleSection = 0;
				$.each(html, function (i, el) {
					if (isScrolledIntoView($(this))) {
						numberOfVisibleSection++;
					}
				});

				if (numberOfVisibleSection > 0) {
					$.each(html, function (i, el) {
						if (isScrolledIntoView($(this))) {
							$('#section_' + i).addClass("sectionHighlight");
							var newerQuery = jQuery.query.SET("section", i);
							window.history.pushState({ path: newerQuery }, '', newerQuery);
						}
						else {
							$('#section_' + i).removeClass("sectionHighlight");
						}
					});
				}
			});

			function isScrolledIntoView(elem) {
				var $elem = $(elem);
				var $window = $(window);

				var docViewTop = $window.scrollTop();
				var docViewBottom = docViewTop + $window.height();

				var elemTop = $elem.offset().top;
				var elemBottom = elemTop + $elem.height();

				return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
			}
		});
	}

	search = () => {

	}

	updateArticleSections() {
		var html = $.parseHTML(this.content);
		var x = 0;
		$('#sections').html(null);
		$.each(html, function (i, el) {
			if (el.nodeName == 'H1' || el.nodeName == 'H2' || el.nodeName == 'H3' || el.nodeName == 'H4' || el.nodeName == 'H5' || el.nodeName == 'H6') {
				$('#sections').append('<div classNAme="section" id="section_' + x + '">' + $(this).text() + "</div > ");
				x++;
			}
		});
		$('#section_0').addClass("sectionHighlight");
	}

	renderNode = node => {
		return (
			<span
				className={classnames('node', {
					'is-active': node === this.state.active
				})}
				onClick={this.onClickNode.bind(null, node)}
				onMouseDown={node.undraggable ? function (e) { e.stopPropagation() } : undefined}
			>
				{node.displayName}
			</span>
		);
	};

	handleChange = tree => {
		this.setState({
			tree: tree
		});
	};

	onClickNode = node => {
		this.setState({
			active: node
		});
		this.sectionName = node.displayName;
		console.log(node);
		if (node.docPath != null) {
			jQuery.query.REMOVE("section");
			this.loadPage(this.gitUrl + '/' + node.docPath, node.docPath, -1);
		}
	};

	handleDropDownChange = e => {
		let docPath = e.target.value;
		this.loadPage(this.gitUrl + '/' + docPath, docPath, -1);
	}

	render() {
		if (this.initialzing) {
			return (<div className="text-center"><img src={require('./image/logo/loading.svg')} id="loadingSvg" style={{ fill: 'red' }} onLoad={() => { /*SVGInject($('#loadingSvg'));*/ }} /></div>);
		}
		let encodedUrl = encodeURIComponent(window.location.href);
		let title = "Quantr Docs - " + this.props.project + " - " + this.sectionName;
		return (
			<div className="project">
				{myConfig.settings.showHeader == "true" && <Header project={this.props.project} type="project" />}
				<div className="mainBody container-fluid">
					{this.docNotAvailable && <div className="alert alert-danger" role="alert">
						Document is not available
					</div>}
					{!this.docNotAvailable && <div className="row">
						<div className="col-12 col-md-2">
							<BrowserView>
								<div className="sticky-top">
									{/* <input id="searchTextField" className="form-control" type="text" placeholder="Search" onChange={this.search} /> */}
									<div className="toc">
										<div className="tree">
											<Tree
												tree={this.state.tree}
												onChange={this.handleChange}
												renderNode={this.renderNode}
											/>
										</div>
									</div>
								</div>
							</BrowserView>
							<MobileView>
								<select className="form-control" onChange={this.handleDropDownChange}>
									{this.state.treeOptions}
								</select>
							</MobileView>
						</div>
						<div className="col-12 col-md-8">
							{this.loading && <div className="text-center"><img src={require('./image/logo/loading.svg')} /></div>}
							<div id="content"></div>
							{!this.loading && <Feedback ref="feedback" url={this.url} project={this.props.project} docPath={this.docPath} />}
							{userCredential.email == 'peter@quantr.hk' &&
								<pre id="debug" style={{ display: 'none', backgroundColor: '#fafafa', border: '1px solid #dee2e6', borderRadius: '5px', padding: '5px' }}></pre>
							}
						</div>
						{isDesktop &&
							<div className="col-12 col-md-2 sectionsCol">
								<div className="sticky-top">
									<div className="artcleIconBar" style={{ fill: myConfig.settings.socialButtonColor }} >
										{/* <a href="/docs" style={{ color: 'black' }}>Quantr Docs Home</a><br /> */}
										<a href={"http://twitter.com/share?text=" + title + "&url=" + `${encodedUrl}`} target="_blank"><img src={require('./image/logo/twitter.svg')} className="articleIcon" style={{ fill: 'red' }} /></a>
										<a href={"mailto:?subject=" + title + "&body=" + `${encodedUrl}`}><img src={require('./image/logo/email.png')} className="articleIcon" /></a>
										{/* <a href={"https://www.linkedin.com/sharing/share-offsite/?url=" + `${encodedUrl}` + "&title=" + title} target="_blank"><img src={require('./image/logo/linkedin.png')} className="articleIcon" /></a> */}
										<a href={"https://www.facebook.com/sharer/sharer.php?t=" + title + "&u=" + `${encodedUrl}`} target="_blank"><img src={require('./image/logo/facebook.png')} className="articleIcon" /></a>
									</div>
									<h1>In this article</h1>
									<div id="sections" data-spy="affix" data-offset-top="197"></div>
								</div>
							</div>
						}
					</div>
					}
				</div>
			</div>
		)
	}
}

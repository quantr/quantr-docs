import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import './Header.module.scss';
import userCredential from 'userCredential';
import * as $ from 'jquery';
import myConfig from 'myConfig';

export default class Header extends Component {
	componentDidMount() {
		particlesJS("particles-js", {
			"particles": {
				"number": {
					"value": 200,
					"density": {
						"enable": true,
						"value_area": 800
					}
				},
				"color": {
					"value": "#ffffff"
				},
				"shape": {
					"type": "circle",
					"stroke": {
						"width": 0,
						"color": "#000000"
					},
					"polygon": {
						"nb_sides": 5
					},
					"image": {
						"src": "",
						"width": 100,
						"height": 190
					}
				},
				"opacity": {
					"value": 0.5,
					"random": false,
					"anim": {
						"enable": false,
						"speed": 1,
						"opacity_min": 0.1,
						"sync": false
					}
				},
				"size": {
					"value": 3,
					"random": true,
					"anim": {
						"enable": false,
						"speed": 40,
						"size_min": 0.1,
						"sync": false
					}
				},
				"line_linked": {
					"enable": true,
					"distance": 150,
					"color": "#ffffff",
					"opacity": 0.4,
					"width": 1
				},
				"move": {
					"enable": true,
					"speed": 6,
					"direction": "none",
					"random": false,
					"straight": false,
					"out_mode": "out",
					"bounce": false,
					"attract": {
						"enable": false,
						"rotateX": 600,
						"rotateY": 1200
					}
				}
			},
			"interactivity": {
				"detect_on": "canvas",
				"events": {
					"onhover": {
						"enable": true,
						"mode": "repulse"
					},
					"onclick": {
						"enable": true,
						"mode": "push"
					},
					"resize": true
				},
				"modes": {
					"grab": {
						"distance": 400,
						"line_linked": {
							"opacity": 0.5
						}
					},
					"bubble": {
						"distance": 400,
						"size": 40,
						"duration": 2,
						"opacity": 8,
						"speed": 3
					},
					"repulse": {
						"distance": 200,
						"duration": 0.4
					},
					"push": {
						"particles_nb": 4
					},
					"remove": {
						"particles_nb": 2
					}
				}
			},
			"retina_detect": true
		});

		if (this.props.type != "main") {
			// $(window).scroll(function () {
			// 	if ($(this).scrollTop() > 10) {
			// 		$('.quantrDocsHeader').addClass("stickyHeader");
			// 	}
			// 	else {
			// 		$('.quantrDocsHeader').removeClass("stickyHeader");
			// 	}
			// });
		}
	}

	render() {
		var menu = [];
		if (userCredential.email == null) {
			menu.push(
				<div className="menu container-fluid">
					<div className="container">
						<a href={myConfig.loginUrl}>Login</a>
					</div>
				</div>
			);
		}
		if (this.props.type == "main") {
			return (
				<div id="particles-js" className="quantrDocsHeader" style={{ height: '300px', color: myConfig.settings.headerColor, background: myConfig.settings.headerBackground }}>
					{menu}
					<div className="mainDiv container-fluid">
						<div className="row">
							<div className="container mainBackground_main">
								<div className="welcome">welcome to</div>
								<div className="banner">Quantr Docs</div>
								<div className="description">Here is our central document repository. Containing all the documents of different project, including SharePoint projects and open source projects. Enjoy !!!</div>
							</div>
						</div>
					</div>
				</div>
			);
		} else {
			return (
				<div id="particles-js" className="quantrDocsHeader" style={{ height: userCredential.email == null ? '140px' : '60px', color: myConfig.settings.headerColor, background: myConfig.settings.headerBackground }}>
					{menu}
					<div className="mainDiv container-fluid">
						<div className="row">
							<div className="container-fluid mainBackground_other" style={{ top: userCredential.email == null ? '60%' : '' }}>
								<div className="homeDiv"><a href={myConfig.homeUrl} style={{ color: myConfig.settings.headerColor }}>Home</a></div>
								<div className="homeDiv">&nbsp;/&nbsp;</div>
								<div className="banner">{this.props.project}</div>
							</div>
							{/* <nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="https://www.quantr.foundation/docs">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">{this.props.project}</li>
								</ol>
							</nav> */}
						</div>
					</div>
				</div>
			);
		}
	}
}

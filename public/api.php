<?
require $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';

if ($_GET['action'] == 'getUrl') {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_GET['url']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);

	//echo $_GET['url'];
	// die;
	if ($_GET['type'] == 'markdown') {
		$Parsedown = new Parsedown();

		// fix email
		$pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
		$replacement = "&lt;$0&gt;";
		$output = preg_replace($pattern, $replacement, $output);
		// end fix email

		// apply graphviz
		// $pattern = "/```mermaid[^`]*```/";
		// $replacement = "FUCK";
		// $output = preg_replace($pattern, $replacement, $output);
		// $output = system("dot -Tpng");
		// end apply graphviz

		echo $Parsedown->text($output);
		// echo $output;
	} else {
		echo $output;
	}
	curl_close($ch);
} elseif ($_GET['action'] == 'getProjectUrl') {
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . qd_project . " where name=%s;", $_GET['name']));
	// echo $wpdb->prepare("SELECT * FROM " . qd_project . " where name=%s;", $_GET['name']);
	echo json_encode($results);
} elseif ($_POST['action'] == 'submitFeedback') {
	if (wp_get_current_user()->user_email == null) {
		return;
	}
	$wpdb->insert(qd_feedback, array(
		"username" => wp_get_current_user()->user_email,
		"date" => current_time('mysql'),
		"url" => $_POST['url'],
		"project" => $_POST['project'],
		"docPath" => $_POST['docPath'],
		"status" => true,
		"content" => $_POST['feedbackContent']
	));
} elseif ($_GET['action'] == 'getFeedbacks') {
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . qd_feedback . " where project=%s and url=%s and docPath=%s and status=true order by date desc;", $_GET['project'], $_GET['url'], $_GET['docPath']));
	echo json_encode($results);
}

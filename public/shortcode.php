<?
function quantr_docs_init()
{
	function quantr_docs_shortcode($atts = [], $content = null)
	{
		ob_start();
		include 'html.php';
		$content = ob_get_clean();
		return $content;
	}
	add_shortcode('quantr_docs', 'quantr_docs_shortcode');
}
add_action('init', 'quantr_docs_init');

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php';
global $wpdb;

if (is_user_logged_in()) {
	$user = [
		'name' => wp_get_current_user()->user_name,
		'email' => wp_get_current_user()->user_email
	];
} else {
	$user = [];
}

if (wp_get_current_user()->user_email == 'peter@quantr.hk') {
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . QUANTR_DOCS_TABLE_PREFIX . "project order by name;", null));
} else {
	$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . QUANTR_DOCS_TABLE_PREFIX . "project where debug=0 order by name;", null));
}
$projects = array();
for ($x = 0; $x < count($results); $x++) {
	$projects[] = array(
		"name" => $results[$x]->name,
		"description" => $results[$x]->description,
		"icon" => $results[$x]->icon,
		"types" => explode(',', $results[$x]->types),
		"url" => $results[$x]->url,
		"enable" => $results[$x]->enable,
		"docFolder" => $results[$x]->docFolder
	);
}

$results = $wpdb->get_results("SELECT * FROM " . qd_setting, OBJECT);
$settings=array();
foreach ($results as &$row) {
	$settings[$row->property]=$row->value;
}
?>

<!-- <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__) ?>/app/assets/css/app.css" type="text/css"> -->
<script type="text/javascript">
	// var STATIC_URL = 'http://localhost/my-app';
	var userCredential = <?= json_encode($user); ?>;
	var projects = <?php echo json_encode($projects); ?>;
	var project = "<?= $_GET['project'] ?>";
	var plugin_dir_url = "<?= plugin_dir_url(__FILE__) ?>";
	var myConfig = {
		homeUrl		:	"https://www.quantr.foundation/docs",
		loginUrl	:	"https://www.quantr.foundation/wp-login.php?redirect_to=https://www.quantr.foundation/docs",
		settings	:	<?= json_encode($settings) ?>
	};
	peterfuckingpath = `<?= plugin_dir_url(__FILE__) ?>/app/assets/bundle/`;
</script>
<style>
	.row:before,
	.row:after {
		display: flex !important;
	}
</style>
<meta property="og:title" content="Quantr Foundation" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://www.quantr.foundation/wp-content/uploads/2023/01/purple.png" />
<?
// $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . QUANTR_DOCS_TABLE_PREFIX . "project where name='".$_GET['project']."';", null));
// var_dump($results);
?>

<div id="app"></div>
<script type="text/javascript" src="<?= plugin_dir_url(__FILE__) ?>/library/particlejs/particles.min.js"></script>
<script type="text/javascript" src="<?= plugin_dir_url(__FILE__) ?>/app/assets/bundle/main.bundle.js"></script>

